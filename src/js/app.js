App = {
  web3Provider: null,
  contracts: {},

  init: function() {
    // Load pets.
    $.getJSON('../items.json', function(data) {
      var itemsRow = $('#itemsRow');
      var itemTemplate = $('#itemTemplate');

      for (i = 0; i < data.length; i ++) {
        itemTemplate.find('.panel-title').text(data[i].name);
        itemTemplate.find('img').attr('src', data[i].picture);
        itemTemplate.find('.item-condition').text(data[i].condition);
        itemTemplate.find('.item-price').text(data[i].price);
        itemTemplate.find('.item-location').text(data[i].location);
        itemTemplate.find('.btn-buy').attr('data-id', data[i].id);
        itemTemplate.find('.btn-buy').attr('data-price', data[i].price);
        itemsRow.append(itemTemplate.html());
      }
    });

    return App.initWeb3();
  },

  initWeb3: function() {
    // Is there an injected web3 instance?
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
    } else {
      // If no injected web3 instance is detected, fall back to Ganache
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }
    web3 = new Web3(App.web3Provider);
      return App.initContract();
  },

  initContract: function() {
    $.getJSON('FleaMarket.json', function(data) {
      // Get the necessary contract artifact file and instantiate it with truffle-contract
      var FleaMarketArtifact = data;
      App.contracts.FleaMarket = TruffleContract(FleaMarketArtifact);

      // Set the provider for our contract
      App.contracts.FleaMarket.setProvider(App.web3Provider);

      // Use our contract to retrieve and mark the adopted pets
      return App.markSold();
    });
    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '.btn-buy', App.handleAdopt);
  },

  markSold: function(adopters, account) {
    var fleaMarketInstance;
    console.log('deploy(): ' + App.contracts.FleaMarket.deployed());
    App.contracts.FleaMarket.deployed().then(function(instance) {
      fleaMarketInstance = instance;
      return fleaMarketInstance.getBuyers.call();
    }).then(function(adopters) {
      for (i = 0; i < adopters.length; i++) {
        if (adopters[i] !== '0x0000000000000000000000000000000000000000') {
          $('.panel-pet').eq(i).find('button').text('Sold').attr('disabled', true);
        }
      }
    }).catch(function(err) {
      console.log(err.message);
    });
  },

  handleAdopt: function(event) {
    console.log('handleAdopt');
    event.preventDefault();

    var itemId = parseInt($(event.target).data('id'));
    var itemPrice = parseFloat($(event.target).data('price'));
    var fleaMarketInstance;

    var itemPriceinWei = web3.toWei(itemPrice);
    console.log('id=' + itemId + ', price=' + itemPrice + '(' + itemPriceinWei + ' wei)');
    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log('got error :' + error);
      }

      var account = accounts[0];
      App.contracts.FleaMarket.deployed().then(function(instance) {
      fleaMarketInstance = instance;

      // Execute adopt as a transaction by sending account
      console.log('account ' + account + 'is buying ' + itemId + ' for ' + itemPriceinWei + ' wei');
      return fleaMarketInstance.buy(itemId, itemPriceinWei, {from: account});

    }).then(function(result) {
      return App.markSold();
    }).catch(function(err) {
      console.log(err.message);
    });
  });
  }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
