mod from truffle pet-shop tutorial.

## Install
```
cd ~/Workspace/flea-market-tutorial
npm install -g truffle
npm install 
```

## Start blockchain

Option 1: for Ubuntu 
```
ganache-cli
``` 
Option 2: launch ganache GUI from OSX

## Compile, deploy, test smart contract
```
truffle compile
truffle migrate
truffle test
truffle migrate --reset
```

## Start light-server, access http://localhost:3000
```
npm run dev
```

