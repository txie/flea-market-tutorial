pragma solidity ^0.4.17;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/FleaMarket.sol";

contract TestFleaMarket {
    FleaMarket fleaMarket = FleaMarket(DeployedAddresses.FleaMarket());
    // Testing buy() function
    function testUserCanAdoptPet() public {
        uint returnedId = fleaMarket.buy(3);
        uint expected = 3;
        Assert.equal(returnedId, expected, "FleaMarket of pet ID 3 should be recorded.");
    }

    // Testing retrieval of a single pet's owner
    function testGetAdopterAddressByPetId() public {
        // Expected owner is this contract
        address expected = this;
        address buyer = fleaMarket.buyers(3);

        Assert.equal(buyer, expected, "Owner of pet ID 3 should be recorded.");
    }

    // Testing retrieval of all pet owners
    function testGetAdopterAddressByPetIdInArray() public {
        // Expected owner is this contract
        address expected = this;

        // Stbuyers in memory rather than contract's storage
        address[16] memory buyers = fleaMarket.getBuyers();
        
        Assert.equal(buyers[3], expected, "Owner of pet ID 3 should be recorded.");
    }

}
