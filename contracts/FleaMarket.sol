pragma solidity ^0.4.17;
contract FleaMarket {
    address[8] public buyers;
    function buy(uint itemId) public returns (uint) {
        require(itemId >= 0 && itemId <= 7);
        buyers[itemId] = msg.sender;
        return itemId;
    }

    function buy(uint itemId, uint priceInWei) public returns (uint) {
        address seller = 0xB8eF9c1c7b14A1D0A1A7728FbC42a2170D0e3AcA;
        require(itemId >= 0 && itemId <= 7);
        require(msg.sender.balance > priceInWei);
        buyers[itemId] = msg.sender;
        bool result = seller.send(priceInWei);
        if (result == true)
            return itemId;
        else
            return 1000;
    }
    // Retrieving the items
    function getBuyers() public view returns (address[8]) {
        return buyers;
    }
}
